# hystrix

#### 项目介绍
使用 hystrix 来完成系统的隔离，服务的降级操作

```
hystrix (hystrix工程)  
  |- product-provider (服务提供者)  
  |- order-consumer (服务消费者,继承HystrixCommand来实现)  
  |- order-consumer-annotation-hystrix (使用@HystrixCommand注解来实现)  
```

##### 使用@HystrixCommand注解实现步骤  
~~~java
1、引入依赖  
    ```
        <dependency>
            <groupId>com.netflix.hystrix</groupId>
            <artifactId>hystrix-javanica</artifactId>
        </dependency>
    ```
 2、配置注解切面
    ```
        @Bean
        public HystrixCommandAspect hystrixCommandAspect() {
            return new HystrixCommandAspect();
        }
    ```
 3、方法编写 参考工程 order-consumer-annotation-hystrix com.huan.hystrix.service.impl.ProductServiceImpl.selectOne方法  
    
 4、注意，如果在有@HystrixCommand注解的类里面使用@Bean来配置bean,那么这个方法需要是静态的配置方式
    @Bean
    public static RestTemplate restTemplate(){
    }
 5、获取导致回退发生的异常，fallback 方法中的第二个参数写 Throwable
    public Product selectOneFallback(String productId, Throwable throwable)
~~~

#### 安装教程 & 使用说明  

1. 启动order-consumer或order-consumer-annotation-hystrix项目，启动类 OrderApplication  
2. 然后访问 POST http://localhost:9098(或9097)/order/create 看返回的数据
3. 启动product-provider项目，启动类ProductApplication
4. 然后访问 POST http://localhost:9098/order/create 看返回的数据是否和上次的访问数据一样。
