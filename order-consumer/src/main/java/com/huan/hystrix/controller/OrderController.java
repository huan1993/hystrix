package com.huan.hystrix.controller;

import com.huan.hystrix.entity.Product;
import com.huan.hystrix.hystrix.NewSelectProductCommand;
import com.huan.hystrix.hystrix.SelectProductCommand;
import com.huan.hystrix.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.UUID;

/**
 * 订单控制器
 *
 * @author huan.fu
 * @date 2018/8/8 - 15:00
 */
@RestController
@RequestMapping("order")
@Slf4j
public class OrderController {

	@Autowired
	private ProductService productService;

	/**
	 * 创建订单
	 *
	 * @return
	 */
	@PostMapping("create")
	public Product createOrder() {
		log.info("====> Thread Name:{}", Thread.currentThread().getName());
		int random = new Random().nextInt(10);
		log.info("{} % 2 == 0 ? {} : {}", random, "NewSelectProductCommand", "SelectProductCommand");
		if (false) {
			return new NewSelectProductCommand(productService, UUID.randomUUID().toString().replace("-", "")).execute();
		} else {
			return new SelectProductCommand(productService, UUID.randomUUID().toString().replace("-", "")).execute();
		}
	}
}
