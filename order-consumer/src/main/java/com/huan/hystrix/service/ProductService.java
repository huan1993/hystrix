package com.huan.hystrix.service;

import com.huan.hystrix.entity.Product;

/**
 * 商品服务
 *
 * @author huan.fu
 * @date 2018/8/8 - 15:25
 */
public interface ProductService {

	/**
	 * 根据商品编号获取商品信息
	 *
	 * @param productId
	 * @return
	 */
	Product selectOne(String productId);

}
