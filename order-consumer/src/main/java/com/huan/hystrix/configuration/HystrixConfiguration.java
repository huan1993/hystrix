package com.huan.hystrix.configuration;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * hystrix 的配置文件
 *
 * @author huan.fu
 * @date 2018/8/10 - 14:26
 */
@Configuration
public class HystrixConfiguration {

	@Bean
	public ServletRegistrationBean hystrixMetricsStreamServlet() {
		return new ServletRegistrationBean(
				new HystrixMetricsStreamServlet(), "/hystrix.stream"
		);
	}
}
