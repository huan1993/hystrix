package com.huan.hystrix.entity;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

/**
 * 商品信息
 *
 * @author huan.fu
 * @date 2018/8/8 - 14:51
 */
@Data
@Builder
public class Product {
	/**
	 * 商品编号
	 */
	private String productId;
	/**
	 * 商品名称
	 */
	private String productName;
	/**
	 * 商品价格
	 */
	private BigDecimal price;

}
