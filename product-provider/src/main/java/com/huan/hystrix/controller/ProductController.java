package com.huan.hystrix.controller;

import com.huan.hystrix.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * 商品控制器
 *
 * @author huan.fu
 * @date 2018/8/8 - 14:52
 */
@RestController
@RequestMapping("product")
@Slf4j
public class ProductController {

	/**
	 * 根据商品编号返回商品信息
	 *
	 * @param productId
	 * @return
	 */
	@GetMapping("{productId}")
	public Product getOne(@PathVariable String productId) throws InterruptedException {
		int seconds = new Random().nextInt(3);
		log.info("线程[{}]休眠[{}]秒.", Thread.currentThread().getName(), seconds);
		TimeUnit.SECONDS.sleep(seconds);
		return Product.builder()
				.productId(productId)
				.productName("苹果笔记本:" + seconds)
				.price(BigDecimal.valueOf(20000))
				.build();
	}

}
